package za.co.stillie.weatherapp.home;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;

import za.co.stillie.weatherapp.R;
import za.co.stillie.weatherapp.common.BaseActivity;
import za.co.stillie.weatherapp.databinding.ActivityHomeBinding;
import za.co.stillie.weatherapp.enums.ResourceStatus;
import za.co.stillie.weatherapp.models.Main;
import za.co.stillie.weatherapp.models.Weather;
import za.co.stillie.weatherapp.models.WeatherResponse;

public class HomeActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

  @Inject
  ViewModelProvider.Factory mViewModelFactory;

  private ActivityHomeBinding mActivityHomeBinding;

  private HomeViewModel mHomeViewModel;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initializeViews();
  }

  /**
   * Init all the views and observers
   */
  private void initializeViews() {
    mActivityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
    setSupportActionBar(mActivityHomeBinding.toolbar);
    mHomeViewModel = ViewModelProviders.of(this, mViewModelFactory).get(HomeViewModel.class);
    initObservers();
    mActivityHomeBinding.swipeToRefresh.setOnRefreshListener(this);

    mActivityHomeBinding.swipeToRefresh.setProgressViewOffset(true, mActivityHomeBinding.toolbar.getHeight(), 160);
    mActivityHomeBinding.swipeToRefresh.setColorSchemeResources(R.color.colorAccent, R.color.colorAccent, R.color.colorAccent, R.color.colorAccent);
    mActivityHomeBinding.dateTextView.setText(getMyTextUtils().getDateTimeNow());
  }

  /**
   * Init all the observables
   */
  private void initObservers() {
    // Observable for the location
    getLocationLiveData().observe(this, aLocation -> mHomeViewModel.setLocation(aLocation));

    // Observable to get the weather data for the location
    mHomeViewModel.getWeatherResult().observe(this, aResult -> {
      if (aResult != null) {
        mActivityHomeBinding.swipeToRefresh.setRefreshing(aResult.getResourceStatus() == ResourceStatus.LOADING);
        if (aResult.getResponse() != null) {
          WeatherResponse response = aResult.getResponse();
          if (response != null) {
            mActivityHomeBinding.locationTextView.setText(response.getName());
            if (response.getWeather() != null) {
              mHomeViewModel.setWeather(response.getWeather());
              setWeather();
            }
            if (response.getMain() != null) {
              mHomeViewModel.setMainWeather(response.getMain());
              setTemps();
            }
          }
        }
      }
    });
  }

  /**
   * This sets the Image for the current conditions as well as a short description of the weather
   */
  private void setWeather() {
    if (mHomeViewModel.getWeather().get(0) != null) {
      Weather weather = mHomeViewModel.getWeather().get(0);
      mActivityHomeBinding.weatherDescriptionTextView.setText(weather.getDescription());
      mActivityHomeBinding.setImageUrl(getImageUtils().getImageUrl(weather.getIcon()));
    }
  }

  /**
   * This method sets the Current temp and the Max and Min Temps
   */
  private void setTemps() {
    Main mainWeather = mHomeViewModel.getMainWeather();

    mActivityHomeBinding.currentTempTextView.setText(
            String.format(getString(R.string.current_temp_text),
                    getMyTextUtils().setTemp(mainWeather.getTemp(), mHomeViewModel.getUnit())
            )
    );
    mActivityHomeBinding.maxTempTextView.setText(
            String.format(getString(R.string.max_temp_text),
                    getMyTextUtils().setTemp(mainWeather.getTempMax(), mHomeViewModel.getUnit())
            )
    );
    mActivityHomeBinding.minTempTextView.setText(
            String.format(getString(R.string.min_temp_text),
                    getMyTextUtils().setTemp(mainWeather.getTempMin(), mHomeViewModel.getUnit())
            )
    );
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_home, menu);
    return true;
  }

  /**
   * We set the text on the toolbar icon to the Unit that the user has selected currently
   */
  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    MenuItem item = menu.findItem(R.id.action_unit);
    if (item != null) {
      item.setTitle(mHomeViewModel.getUnit().getUnitCode());
    }
    return super.onPrepareOptionsMenu(menu);
  }

  /**
   * When ever we change the unit, we need to fetch the data from the webservice
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_unit:
        mHomeViewModel.toggleUnit();
        mHomeViewModel.fetchData();
        break;
    }
    invalidateOptionsMenu();
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onRefresh() {
    startLocationPermissionRequest();
  }
}
