package za.co.stillie.weatherapp.utilities;

import android.content.Context;
import android.text.format.DateUtils;

import java.util.Date;

import javax.inject.Inject;

import za.co.stillie.weatherapp.enums.Unit;

import static android.text.format.DateUtils.FORMAT_ABBREV_MONTH;
import static android.text.format.DateUtils.FORMAT_SHOW_DATE;
import static android.text.format.DateUtils.FORMAT_SHOW_TIME;
import static android.text.format.DateUtils.FORMAT_SHOW_WEEKDAY;
import static android.text.format.DateUtils.FORMAT_SHOW_YEAR;

public class MyTextUtils {

  private Context mContext;

  @Inject
  public MyTextUtils(Context aContext) {
    mContext = aContext;
  }

  public String setTemp(double aTemp, Unit aUnit) {
    return roundUpDouble(aTemp) + aUnit.getUnitCode();
  }

  private Double roundUpDouble(double aValue) {
    return Math.round(aValue * 100.0) / 100.0;
  }

  public String getDateTimeNow() {
    return DateUtils.formatDateTime(mContext, new Date().getTime(),
            FORMAT_SHOW_WEEKDAY |
                    FORMAT_SHOW_DATE |
                    FORMAT_SHOW_YEAR |
                    FORMAT_ABBREV_MONTH |
                    FORMAT_SHOW_TIME);
  }

}
