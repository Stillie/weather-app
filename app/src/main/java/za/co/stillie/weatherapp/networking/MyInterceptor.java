package za.co.stillie.weatherapp.networking;

import java.io.IOException;

import javax.annotation.Nonnull;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A OkHTTP Interceptor class, that Retrofit uses to build a HTTP request
 */
public class MyInterceptor implements Interceptor {

  /**
   * A manager that provides the correct URL
   */
  private NetworkManager mNetworkManager;

  public MyInterceptor(NetworkManager aNetworkManager) {
    mNetworkManager = aNetworkManager;
  }

  /**
   * Intercepts the request, and builds it, adding headers and a base url
   *
   * @param aChain a Chain
   * @return a OkHTT 3 Response
   * @throws IOException an exception
   */
  @Override
  public Response intercept(@Nonnull Chain aChain) throws IOException {
    Request request = aChain.request();

    HttpUrl httpUrl = HttpUrl.parse(getBaseUrl());
    if (httpUrl == null) {
      httpUrl = HttpUrl.parse(getBaseUrl());
    }

    if (httpUrl != null && httpUrl.host() != null) {
      HttpUrl newUrl = request.url().newBuilder()
              .scheme("http")
              .host(httpUrl.host())
              .build();
      Request.Builder requestBuilder = setup(request, newUrl);
      request = requestBuilder.build();
    }
    return aChain.proceed(request);
  }

  /**
   * Builds a request based on the url provided - which includes adding our common HTTP headers
   *
   * @param aRequest the origin request
   * @param aNewUrl   a url to form the baseUrl
   * @return a request builder
   */
  private Request.Builder setup(Request aRequest, HttpUrl aNewUrl) {
    return aRequest.newBuilder()
            .url(aNewUrl)
            .addHeader("Accept", "application/json")
            .addHeader("x-api-key", NetworkConstants.WS_API_KEY);
  }

  /**
   * @return A String, which is the current base URL of the app
   */
  public String getBaseUrl() {
    return mNetworkManager.getBaseUrl();
  }
}
