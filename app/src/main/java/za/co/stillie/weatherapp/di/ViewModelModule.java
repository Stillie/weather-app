package za.co.stillie.weatherapp.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import za.co.stillie.weatherapp.home.HomeViewModel;
import za.co.stillie.weatherapp.utilities.MyViewModeFactory;

/**
 * A module class that provides various ViewModels to the rest of the application via Dagger
 */
@Module
abstract class ViewModelModule {
  @Binds
  abstract ViewModelProvider.Factory bindsViewModelFactory(MyViewModeFactory aMyViewModeFactory);

  @Binds
  @IntoMap
  @ViewModelKey(HomeViewModel.class)
  abstract ViewModel bindsHomeViewModel(HomeViewModel aHomeViewModel);
}
