package za.co.stillie.weatherapp.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;
import za.co.stillie.weatherapp.Constants;
import za.co.stillie.weatherapp.application.MyApplication;

/**
 * Application scope for Dagger.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AndroidInjectionModule.class,
        ActivityBuilder.class,
        AppModule.class,
        ViewModelModule.class})
public interface AppComponent {

  /**
   * Injects the main application class
   *
   * @param aMyApplication MyApplication class
   */
  void inject(MyApplication aMyApplication);

  /**
   * Exposes a the Application Context to other components
   *
   * @return a Application Context
   */
  @Named(Constants.APP_CONTEXT)
  Context getContext();

  /**
   * The builder for this component
   */
  @Component.Builder
  interface Builder {
    @BindsInstance
    Builder application(Application aApplication);

    AppComponent build();
  }
}
