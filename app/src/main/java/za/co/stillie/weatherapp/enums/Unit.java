package za.co.stillie.weatherapp.enums;

import java.util.NoSuchElementException;

/**
 * Enum to manage the Unit the weather should be displayed in
 */
public enum Unit {

  KELVIN(1, "kelvin", "Kelvin", (char) 0x00B0 + "K"),
  IMPERIAL(2, "imperial", "Imperial", (char) 0x00B0 + "F"),
  METRIC(3, "metric", "Metric", (char) 0x00B0 + "C");

  private final int mId;
  private final String mShortName;
  private final String mDisplayName;
  private final String mUnitCode;

  Unit(int aId, String aShortName, String aDisplayName, String aUnitCode) {
    mId = aId;
    mShortName = aShortName;
    mDisplayName = aDisplayName;
    mUnitCode = aUnitCode;
  }

  public String getUnitCode() {
    return mUnitCode;
  }

  /**
   * Returns the Unit for the given ItemId
   * or throws a NoSuchElementException.
   *
   * @param aItemId - Id for the Unit Type
   * @return - The Unit in relation to the ID
   * @throws NoSuchElementException if no element was found
   */
  public static Unit getValue(int aItemId) throws NoSuchElementException {
    Unit value = getValueOrNull(aItemId);
    if (value == null) {
      throw new NoSuchElementException("No Unit with id: " + aItemId);
    }
    return value;
  }

  /**
   * Returns the Unit for the given ItemId
   * or returns null.
   *
   * @param aItemId - Id for the Unit Type
   * @return - The Unit in relation to the ID or null
   */
  public static Unit getValueOrNull(int aItemId) {
    switch (aItemId) {
      case 1:
        return KELVIN;
      case 2:
        return IMPERIAL;
      case 3:
        return METRIC;
      default:
        return null;
    }
  }

  /**
   * Returns the Unit for the given ItemId
   * or returns the default.
   *
   * @param aItemId - Id for the Unit Type
   * @return - The Unit for in relation to the ID or a default value
   */
  public static Unit getValueOrDefault(int aItemId, Unit aDefaultValue) {
    Unit value = getValueOrNull(aItemId);
    return value != null ? value : aDefaultValue;
  }

  public int getId() {
    return mId;
  }

  public String getShortName() {
    return mShortName;
  }

  public String getDisplayName() {
    return mDisplayName;
  }
}
