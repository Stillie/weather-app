package za.co.stillie.weatherapp.di;

import dagger.Module;

/**
 * A service-specific module, used for DI. Everything 'provided' here will be accessible to injected Services, using the ServiceInjector class
 */
@Module
class ServiceModule {
}
