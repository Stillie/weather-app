package za.co.stillie.weatherapp.di;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import dagger.android.AndroidInjection;
import dagger.android.HasActivityInjector;
import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;
import za.co.stillie.weatherapp.application.MyApplication;

/**
 * Provides "automatic" injection for activities and fragments as well as injects the main application class
 */
public class AppInjector {

  public static AppComponent init(MyApplication aMyApplication) {

    AppComponent appComponent = DaggerAppComponent.builder().application(aMyApplication).build();
    appComponent.inject(aMyApplication);

    aMyApplication.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
      @Override
      public void onActivityCreated(Activity aActivity, Bundle aBundle) {
        handleActivity(aActivity);
      }

      @Override
      public void onActivityStarted(Activity aActivity) {

      }

      @Override
      public void onActivityResumed(Activity aActivity) {

      }

      @Override
      public void onActivityPaused(Activity aActivity) {

      }

      @Override
      public void onActivityStopped(Activity aActivity) {

      }

      @Override
      public void onActivitySaveInstanceState(Activity aActivity, Bundle aBundle) {

      }

      @Override
      public void onActivityDestroyed(Activity aActivity) {

      }
    });
    return appComponent;
  }

  private static void handleActivity(Activity aActivity) {
    if (aActivity instanceof HasActivityInjector) {
      Timber.d("Injected Activity: s", aActivity.getClass().getName());
      AndroidInjection.inject(aActivity);
    }
    if (aActivity instanceof FragmentActivity) {
      ((FragmentActivity) aActivity).getSupportFragmentManager()
              .registerFragmentLifecycleCallbacks(new FragmentManager.FragmentLifecycleCallbacks() {
                @Override
                public void onFragmentCreated(FragmentManager aFragmentManager, Fragment aFragment, Bundle aSavedInstanceState) {
                  if (aFragment instanceof Injectable) {
                    Timber.d("Injected Fragment: s", aFragment.getClass().getName());
                    AndroidSupportInjection.inject(aFragment);
                  }
                }
              }, true);
    }
  }
}
