package za.co.stillie.weatherapp.networking;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import timber.log.Timber;
import za.co.stillie.weatherapp.application.AppExecutors;
import za.co.stillie.weatherapp.models.Resource;

/**
 * A generic class that provides a resource backed by the network.
 *
 * @param <T> ResultType type
 */
public abstract class NetworkBoundResource<T> {

  /**
   * An executors class, that executes code on different threads
   */
  @Nullable
  private AppExecutors mAppExecutors;

  /**
   * The LiveData observable, that observers use, to observe the different network states (Loading, Success, Error)
   */
  private final MediatorLiveData<Resource<T>> mResult = new MediatorLiveData<>();

  /**
   * Constructor for this class
   *
   * @param aAppExecutors an executors class that executes code on different threads
   *                      NOTE: if it is not passed in to this constructor, saveCallResult will execute on current thread!
   */
  @MainThread
  protected NetworkBoundResource(@Nullable AppExecutors aAppExecutors) {
    mAppExecutors = aAppExecutors;
    fetchFromNetwork();
  }

  /**
   * A constructor that takes no arguments - used for when we don't need a AppExecutors instance
   */
  @MainThread
  protected NetworkBoundResource() {
    fetchFromNetwork();
  }

  /**
   * Does an API request, and lets all observers
   * know about onChanged events
   */
  private void fetchFromNetwork() {

    LiveData<ApiResponse<T>> apiResponse = createCall();

    mResult.setValue(Resource.loading());

    mResult.addSource(apiResponse, response -> {

      mResult.removeSource(apiResponse);

      if (response != null) {
        if (response.isSuccessful()) {

          if (mAppExecutors != null) {
            mAppExecutors.getDiskIO().execute(() -> saveCallResult(processResponse(response)));
          } else {
            saveCallResult(processResponse(response));
          }

          mResult.setValue(Resource.success(null, response));

        } else {
          mResult.setValue(Resource.errors(response.getCode()));
        }
      }
    });
  }

  /**
   * Gets the LiveData observable, to observe the different network states (Loading, Success, Error)
   *
   * @return a LiveData<Resource<T> observable
   */
  public LiveData<Resource<T>> asLiveData() {
    return mResult;
  }

  /*
   * Classes that implement this method, can use
   * this method, if they which to save any part of the
   * deserialized response - typically, this is to a DB
   *
   * Note that this method runs on a background thread!
   */
  @WorkerThread
  protected void saveCallResult(@NonNull T aItem) {
  }

  /**
   * Processes the response and returns a deserialized object (The body)
   *
   * @param aResponse The successful response
   * @return a deserialized response object
   */
  @WorkerThread
  private T processResponse(ApiResponse<T> aResponse) {
    return aResponse.getBody();
  }

  /**
   * Implement this method in a repository. You must return a LiveData<ApiResponse<T> observable
   * This is the API call, that the class will make and that you use asLiveData() to observe the changes on
   *
   * @return a LiveData<ApiResponse<T> observable
   */
  @NonNull
  @MainThread
  protected abstract LiveData<ApiResponse<T>> createCall();
}
