package za.co.stillie.weatherapp.di;

import za.co.stillie.weatherapp.application.MyApplication;

/**
 * An injector class, to handle injecting Services.
 */
public class ServiceInjector {

  /**
   * Builds the component as part of dagger
   *
   * @return a ServiceComponent
   */
  private static ServiceComponent getServiceComponent() {
    return DaggerServiceComponent.builder()
            .appComponent(MyApplication.getInstance().getAppComponent())
            .build();
  }
}
