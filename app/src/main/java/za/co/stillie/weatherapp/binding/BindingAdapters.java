package za.co.stillie.weatherapp.binding;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Data Binding adapters specific to the app.
 */
public class BindingAdapters {

  /**
   * Changes the current views viability status
   *
   * @param aView a view to change statuses on
   * @param aShow a boolean to set if the view is viable or not
   */
  @BindingAdapter("visibleGone")
  public static void showHide(View aView, boolean aShow) {
    aView.setVisibility(aShow ? View.VISIBLE : View.GONE);
  }


  /**
   * This method is used for binding an image URL to a view in the XML file.
   * <p>
   *
   * @param aImageView - The ImageView to apply the URL to
   * @param aUrl       - The URL of the image to display in the ImageView
   */
  @BindingAdapter({"imageUrl"})
  public static void loadThumbnailImage(ImageView aImageView, String aUrl) {
    if (!TextUtils.isEmpty(aUrl)) {
      Glide.with(aImageView.getContext())
              .load(aUrl)
              .apply(new RequestOptions())
              .into(aImageView);
    }
  }
}
