package za.co.stillie.weatherapp.utilities;

import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;
import za.co.stillie.weatherapp.Constants;
import za.co.stillie.weatherapp.R;
import za.co.stillie.weatherapp.application.MyApplication;
import za.co.stillie.weatherapp.interfaces.HttpStatus;
import za.co.stillie.weatherapp.networking.ApiResponse;

public class LiveDataCallAdapter<T> implements CallAdapter<T, LiveData<ApiResponse<T>>> {

  private final Type mResponseType;
  private final MyApplication mApplication = MyApplication.getInstance();
  private Intent mBroadcastIntent;

  LiveDataCallAdapter(Type aResponseType) {
    mResponseType = aResponseType;
  }

  @Override
  public Type responseType() {
    return mResponseType;
  }

  /**
   * Handles the response from the API and then checks to see if its an error or success
   * After that, it posts to all observers the APIResponse
   *
   * @param call The request call being made
   * @return a LiveData observable with ApiResponse<R< as the type parameter
   */
  @Override
  public LiveData<ApiResponse<T>> adapt(@NonNull Call<T> call) {

    return new LiveData<ApiResponse<T>>() {

      final AtomicBoolean started = new AtomicBoolean(false);

      //Called when the number of observers go from 0 to 1
      @Override
      protected void onActive() {
        super.onActive();
        if (started.compareAndSet(false, true)) {

          call.enqueue(new Callback<T>() {

            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
              ApiResponse<T> apiResponse = new ApiResponse<>(response);
              if (!apiResponse.isSuccessful()) {
                handledUnsuccessfulResponse(response, apiResponse);
              }
              postValue(apiResponse);
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable throwable) {
              //TODO: Handle complete network failures.
              Timber.e("BobeTAG: " + throwable.getMessage());
            }
          });
        }
      }
    };
  }

  /**
   * Handles all network responses whose response code is not in the [200..300) range.
   * Also sends out a Local Broadcast for HTTP errors that are not bad requests or unauthorized
   */
  private void handledUnsuccessfulResponse(Response<T> aResponse, ApiResponse aApiResponse) {

    switch (aResponse.code()) {
      case HttpStatus.INTERNAL_SERVER_ERROR:
        mBroadcastIntent = new Intent(Constants.Local_Broadcast_Show_Dialog);
        break;
      case HttpStatus.UNAUTHORIZED:
        mBroadcastIntent = new Intent(Constants.Local_Broadcast_Unauthorized);
        break;
      case HttpStatus.NOT_FOUND:
        mBroadcastIntent = new Intent(Constants.Local_Broadcast_Show_Dialog);
        mBroadcastIntent.putExtra("message", mApplication.getString(R.string.error_resource_not_found));
        break;
      case HttpStatus.UNAVAILABLE:
        mBroadcastIntent = new Intent(Constants.Local_Broadcast_Show_Dialog);
        mBroadcastIntent.putExtra("message", mApplication.getString(R.string.error_service_unavailable));
        break;
      case HttpStatus.FORBIDDEN:
        mBroadcastIntent = new Intent(Constants.Local_Broadcast_Show_Dialog);
        mBroadcastIntent.putExtra("message", mApplication.getString(R.string.error_service_forbidden));
        break;
      case HttpStatus.BAD_REQUEST:
        break;
      default:
        showGenericErrorDialog();
        break;
    }

    if (mBroadcastIntent != null) {
      if (mBroadcastIntent.getExtras() != null) {
        LocalBroadcastManager.getInstance(mApplication).sendBroadcast(mBroadcastIntent);
      }
    }
  }

  /**
   * Sends a local broadcast to all listeners, with the error message
   */
  private void showGenericErrorDialog() {
    mBroadcastIntent = new Intent(Constants.Local_Broadcast_Show_Dialog);
    mBroadcastIntent.putExtra("message", mApplication.getResources().getString(R.string.error_generic_server_error));
    LocalBroadcastManager.getInstance(mApplication).sendBroadcast(mBroadcastIntent);
  }
}