package za.co.stillie.weatherapp.application;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;
/**
 * Global executor pools for the whole application.
 * <p>
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
@Singleton
public class AppExecutors {

  private final Executor mDiskIO;
  private final Executor mNetworkIO;
  private final Executor mMainThread;

  private AppExecutors(Executor aDiskIO,
                      Executor aNetworkIO,
                      Executor aMainThread) {
    mDiskIO = aDiskIO;
    mNetworkIO = aNetworkIO;
    mMainThread = aMainThread;
  }

  @Inject
  AppExecutors(){
    this(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3), new MainThreadExecutor());
  }

  public Executor getDiskIO() {
    return mDiskIO;
  }

  public Executor getNetworkIO() {
    return mNetworkIO;
  }

  public Executor getMainThread() {
    return mMainThread;
  }

  private static class MainThreadExecutor implements Executor {
    private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    @Override
    public void execute(@NonNull Runnable aRunnable) {
      mainThreadHandler.post(aRunnable);
    }
  }
}
