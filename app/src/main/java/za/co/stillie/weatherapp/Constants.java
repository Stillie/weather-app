package za.co.stillie.weatherapp;

public class Constants {


  //region Broadcast intents
  public static final String Local_Broadcast_Show_Dialog = "showAlertDialog";
  public static final String Local_Broadcast_Unauthorized = "unauthorized";
  public static final String APP_CONTEXT = "app_context";
  //endregion

  public static final String KEY_UNIT = "sp_key_unit";
  public static final String SHARED_PREF_FILE_NAME = "WeatherAppPrefs";

}
