package za.co.stillie.weatherapp.home;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import za.co.stillie.weatherapp.application.AppExecutors;
import za.co.stillie.weatherapp.models.Resource;
import za.co.stillie.weatherapp.models.UserLocation;
import za.co.stillie.weatherapp.models.WeatherResponse;
import za.co.stillie.weatherapp.networking.ApiResponse;
import za.co.stillie.weatherapp.networking.MyApiService;
import za.co.stillie.weatherapp.networking.NetworkBoundResource;

class HomeRepository {

  private final MyApiService mMyApiService;
  private final AppExecutors mAppExecutors;

  @Inject
  HomeRepository(MyApiService aMyApiService, AppExecutors aAppExecutors) {
    mMyApiService = aMyApiService;
    mAppExecutors = aAppExecutors;
  }

  /**
   * Gets the weather data for the current location of the device
   *
   * @param aUserLocation - User location object
   * @return - The response
   */
  LiveData<Resource<WeatherResponse>> getCurrentLocationWeather(UserLocation aUserLocation) {
    return new NetworkBoundResource<WeatherResponse>(mAppExecutors) {

      @NonNull
      @Override
      protected LiveData<ApiResponse<WeatherResponse>> createCall() {
        return mMyApiService.getHomeWeather(
                aUserLocation.getLatitude(),
                aUserLocation.getLongitude(),
                aUserLocation.getUnit().getShortName()
        );
      }

    }.asLiveData();
  }


}
