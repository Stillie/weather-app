package za.co.stillie.weatherapp.networking;

import android.arch.lifecycle.LiveData;

import retrofit2.http.GET;
import retrofit2.http.Query;
import za.co.stillie.weatherapp.models.WeatherResponse;

/**
 * An interface class that lists all APIs and their request params
 * This class is used in repositories and injected with Dagger
 */
public interface MyApiService {

  /**
   * Used to get the Weather response of the users location
   *
   * @return a LiveData object, that has a WeatherResponse as part of the APIResponse
   */
  @GET(NetworkConstants.WS_LOCATION_WEATHER)
  LiveData<ApiResponse<WeatherResponse>> getHomeWeather(@Query("lat") String aLatitude, @Query("lon") String aLongitude, @Query("units") String
          aUnit);
}
