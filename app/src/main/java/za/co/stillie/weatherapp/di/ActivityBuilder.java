package za.co.stillie.weatherapp.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import za.co.stillie.weatherapp.home.HomeActivity;

/**
 * A Builder class, that provides dependency injection into the activities
 */
@Module(includes = FragmentBuildersModule.class)
abstract class ActivityBuilder {

  @ContributesAndroidInjector
  abstract HomeActivity contributesHomeActivity();
}
