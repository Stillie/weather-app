package za.co.stillie.weatherapp.utilities;

import javax.inject.Inject;

public class ImageUtils {

  @Inject
  public ImageUtils() {
  }

  /**
   * Gets the Image Url to display the Weather Image
   * @param aIconCode - Code returned from the webservice used to
   *                  populate the URL to get the image
   * @return - Populated Image Url
   */
  public String getImageUrl(String aIconCode) {
    String url = "http://openweathermap.org/img/w/%s.png";
    return String.format(url, aIconCode);
  }
}
