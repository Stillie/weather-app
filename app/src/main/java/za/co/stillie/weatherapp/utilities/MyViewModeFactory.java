package za.co.stillie.weatherapp.utilities;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

public class MyViewModeFactory implements ViewModelProvider.Factory {

  private final Map<Class<? extends ViewModel>, Provider<ViewModel>> mCreators;

  @Inject
  MyViewModeFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> aCreators) {
    mCreators = aCreators;
  }


  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
    Provider<? extends ViewModel> creator = mCreators.get(modelClass);

    if (creator != null) {
      for (Map.Entry<Class<? extends ViewModel>, Provider<ViewModel>> entry : mCreators.entrySet()) {
        if (modelClass.isAssignableFrom(entry.getKey())) {
          creator = entry.getValue();
          break;
        }
      }
    }

    if (creator == null) {
      throw new IllegalArgumentException("unknown model class " + modelClass.getName());
    }

    try {
      //noinspection unchecked
      return (T) creator.get();
    } catch (Exception aE) {
      throw new RuntimeException(aE);
    }
  }
}
