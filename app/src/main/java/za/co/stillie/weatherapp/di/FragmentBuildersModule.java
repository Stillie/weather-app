package za.co.stillie.weatherapp.di;

import dagger.Module;

/**
 * A Builder class, that provides dependency injection into the fragments
 */
@Module
class FragmentBuildersModule {
}
