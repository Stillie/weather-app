package za.co.stillie.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main {

  @SerializedName("temp")
  @Expose
  private double mTemp;

  @SerializedName("pressure")
  @Expose
  private double mPressure;

  @SerializedName("humidity")
  @Expose
  private int mHumidity;

  @SerializedName("temp_min")
  @Expose
  private double mTempMin;

  @SerializedName("temp_max")
  @Expose
  private double mTempMax;

  @SerializedName("grnd_level")
  @Expose
  private double mGrndLevel;

  @SerializedName("sea_level")
  @Expose
  private double mSeaLevel;

  public void setPressure(double aPressure) {
    mPressure = aPressure;
  }

  public double getTempMax() {
    return mTempMax;
  }

  public void setTempMax(double aTempMax) {
    mTempMax = aTempMax;
  }

  public double getGrndLevel() {
    return mGrndLevel;
  }

  public void setGrndLevel(double aGrndLevel) {
    mGrndLevel = aGrndLevel;
  }

  public double getSeaLevel() {
    return mSeaLevel;
  }

  public void setSeaLevel(double aSeaLevel) {
    mSeaLevel = aSeaLevel;
  }

  public double getTemp() {
    return mTemp;
  }

  public void setTemp(double temp) {
    mTemp = temp;
  }

  public int getHumidity() {
    return mHumidity;
  }

  public void setHumidity(int humidity) {
    mHumidity = humidity;
  }

  public double getTempMin() {
    return mTempMin;
  }

  public void setTempMin(double tempMin) {
    mTempMin = tempMin;
  }

  public double getPressure() {
    return mPressure;
  }
}
