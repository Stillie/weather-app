package za.co.stillie.weatherapp.models;

import za.co.stillie.weatherapp.enums.Unit;

public class UserLocation {

  private String mLongitude;
  private String mLatitude;
  private Unit unit;

  public Unit getUnit() {
    return unit;
  }

  public void setUnit(Unit aUnit) {
    unit = aUnit;
  }

  public String getLongitude() {
    return mLongitude;
  }

  public void setLongitude(String aLongitude) {
    mLongitude = aLongitude;
  }

  public String getLatitude() {
    return mLatitude;
  }

  public void setLatitude(String aLatitude) {
    mLatitude = aLatitude;
  }
}
