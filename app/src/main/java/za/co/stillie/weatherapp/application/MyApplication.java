package za.co.stillie.weatherapp.application;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.ProcessLifecycleOwner;

import com.facebook.stetho.Stetho;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;
import za.co.stillie.weatherapp.BuildConfig;
import za.co.stillie.weatherapp.di.AppComponent;
import za.co.stillie.weatherapp.di.AppInjector;

@Singleton
public class MyApplication extends Application implements HasActivityInjector {

  private AppComponent mAppComponent;

  @Inject
  DispatchingAndroidInjector<Activity> mDispatchingAndroidInjector;

  @Inject
  AppExecutors mAppExecutors;

  private static MyApplication sInstance;

  @Override
  public void onCreate() {
    super.onCreate();
    initDagger();
    sInstance = this;
    initStetho();
    initTimber();
    ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifeCycleListener());
  }

  /**
   * Initialize Timber
   */
  private void initTimber() {
    Timber.plant(new Timber.DebugTree());
  }

  /**
   * Initialize Stetho
   */
  private void initStetho() {
    if (BuildConfig.DEBUG) {
      Stetho.initializeWithDefaults(this);
    }
  }

  /**
   * Initialise Dagger
   */
  private void initDagger() {
    mAppComponent = AppInjector.init(this);
  }

  @Override
  public DispatchingAndroidInjector<Activity> activityInjector() {
    return mDispatchingAndroidInjector;
  }

  public AppComponent getAppComponent() {
    return mAppComponent;
  }

  public static MyApplication getInstance() {
    return sInstance;
  }
}
