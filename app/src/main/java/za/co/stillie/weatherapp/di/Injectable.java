package za.co.stillie.weatherapp.di;

/**
 * An interface class, that all Fragments need to implement,
 * so the fragment can be injected, using Dagger
 */
interface Injectable {
}
