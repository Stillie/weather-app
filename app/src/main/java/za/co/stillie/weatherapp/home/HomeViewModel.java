package za.co.stillie.weatherapp.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.location.Location;

import java.util.List;

import javax.inject.Inject;

import za.co.stillie.weatherapp.Constants;
import za.co.stillie.weatherapp.common.MyViewModel;
import za.co.stillie.weatherapp.enums.Unit;
import za.co.stillie.weatherapp.models.Main;
import za.co.stillie.weatherapp.models.Resource;
import za.co.stillie.weatherapp.models.UserLocation;
import za.co.stillie.weatherapp.models.Weather;
import za.co.stillie.weatherapp.models.WeatherResponse;
import za.co.stillie.weatherapp.utilities.SharedPreferencesUtils;

public class HomeViewModel extends MyViewModel {

  private final LiveData<Resource<WeatherResponse>> mWeatherResult;
  private final SharedPreferencesUtils mSharedPreferencesUtils;

  private final MutableLiveData<UserLocation> mWeatherQuery = new MutableLiveData<>();

  private Unit mUnit;
  private Main mMainWeather;
  private List<Weather> mWeather;
  private UserLocation mUserLocation = new UserLocation();

  @Inject
  HomeViewModel(HomeRepository aHomeRepository, SharedPreferencesUtils aSharedPreferencesUtils) {
    mWeatherResult = Transformations.switchMap(mWeatherQuery, aHomeRepository::getCurrentLocationWeather);
    mSharedPreferencesUtils = aSharedPreferencesUtils;

    observeLiveData(mWeatherResult);
  }

  /**
   * This method fetches the data from the webservice as well as gets the Unit that the user has set before sending to the webservice
   */
  void fetchData() {
    mUserLocation.setUnit(getUnit());
    mWeatherQuery.setValue(mUserLocation);
  }

  /**
   * This method toggles the unit when the toolbar action is clicked
   */
  void toggleUnit() {
    switch (mUnit) {
      case IMPERIAL:
        setUnit(Unit.KELVIN);
        break;
      case METRIC:
        setUnit(Unit.IMPERIAL);
        break;
      case KELVIN:
        setUnit(Unit.METRIC);
      default:
    }
  }


  // region Getters and Setters

  /**
   * Get the observer to pull the response out.
   */
  LiveData<Resource<WeatherResponse>> getWeatherResult() {
    return mWeatherResult;
  }

  /**
   * Set the current location of the device
   *
   * @param aLocation - Location object from the last device location
   */
  void setLocation(Location aLocation) {
    mUserLocation.setLatitude(String.valueOf(aLocation.getLatitude()));
    mUserLocation.setLongitude(String.valueOf(aLocation.getLongitude()));
    mUserLocation.setUnit(getUnit());
    fetchData();
  }

  /**
   * Get the Unit the user has selected.
   * <p>
   * If there is no unit set, we get the unit that is saved in the sharedprefs
   *
   * @return - Unit Object
   */
  Unit getUnit() {
    if (mUnit == null) {
      mUnit = Unit.getValueOrDefault(mSharedPreferencesUtils.getInt(Constants.KEY_UNIT), Unit.METRIC);
    }
    return mUnit;
  }

  /**
   * Set the unit.
   * Saves the unit to the SharedPrefs so that the unit stays to what the user had selected before.
   * <p>
   * Its used to restore previous settings when the user closes the application
   *
   * @param aUnit - Unit
   */
  private void setUnit(Unit aUnit) {
    mUnit = aUnit;
    mSharedPreferencesUtils.putInt(Constants.KEY_UNIT, mUnit.getId());
  }

  /**
   * Used to get the Main Weather Object
   *
   * @return - Main Weather Object
   */
  Main getMainWeather() {
    return mMainWeather;
  }

  /**
   * Set the main weather object
   *
   * @param aMainWeather - Main Weather Object
   */
  void setMainWeather(Main aMainWeather) {
    mMainWeather = aMainWeather;
  }

  /**
   * Gets the list of Weather Objects
   *
   * @return - List of Weather Objects
   */
  List<Weather> getWeather() {
    return mWeather;
  }

  /**
   * Sets the List of Weather objects
   *
   * @param aWeather - List of Weather Objects
   */
  void setWeather(List<Weather> aWeather) {
    mWeather = aWeather;
  }
  // endregion
}
