package za.co.stillie.weatherapp.utilities;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.annotation.Nullable;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import za.co.stillie.weatherapp.networking.ApiResponse;

/**
 * A factory method used to build our LiveCallAdaptor
 * This class will throw IllegalArgumentExceptions when conditions are not
 * correct to build the calling adapter
 */
public class LiveDataCallAdapterFactory extends CallAdapter.Factory {

  /**
   * Constructor used to build the calling adapter class
   *
   * @param aReturnType  The return type
   * @param aAnnotations The annotations
   * @param aRetrofit    The current Retrofit instance
   * @return a CallAdaptor (LiveDataCallAdaptor )
   */
  @Nullable
  @Override
  public CallAdapter<?, ?> get(@NonNull Type aReturnType, @NonNull Annotation[] aAnnotations, @NonNull Retrofit aRetrofit) {

    if (getRawType(aReturnType) != LiveData.class) {
      return null;
    }

    Type observableType = getParameterUpperBound(0, (ParameterizedType) aReturnType);
    Class<?> rawObservableType = getRawType(observableType);

    if (rawObservableType != ApiResponse.class) {
      throw new IllegalArgumentException("type must be a resource");
    }
    if (!(observableType instanceof ParameterizedType)) {
      throw new IllegalArgumentException("resource must be parametrized");
    }

    Type bodyType = getParameterUpperBound(0, (ParameterizedType) observableType);
    return new LiveDataCallAdapter<>(bodyType);

  }
}
