package za.co.stillie.weatherapp.di;

import android.app.Application;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import za.co.stillie.weatherapp.BuildConfig;
import za.co.stillie.weatherapp.Constants;
import za.co.stillie.weatherapp.networking.MyApiService;
import za.co.stillie.weatherapp.networking.MyInterceptor;
import za.co.stillie.weatherapp.networking.NetworkConstants;
import za.co.stillie.weatherapp.networking.NetworkManager;
import za.co.stillie.weatherapp.utilities.ImageUtils;
import za.co.stillie.weatherapp.utilities.LiveDataCallAdapterFactory;
import za.co.stillie.weatherapp.utilities.MyTextUtils;
import za.co.stillie.weatherapp.utilities.SharedPreferencesUtils;

import static android.content.Context.MODE_PRIVATE;

@Module(includes = ViewModelModule.class)
class AppModule {

  @Singleton
  @Provides
  MyApiService providesApiService(OkHttpClient aOkHttpClient, MyInterceptor aMyInterceptor) {
    return new Retrofit.Builder()
            .baseUrl(aMyInterceptor.getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(new LiveDataCallAdapterFactory())
            .callFactory(aOkHttpClient)
            .build()
            .create(MyApiService.class);
  }

  @Singleton
  @Provides
  OkHttpClient providesOkHttpClient(MyInterceptor aMyInterceptor) {
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    httpClient.connectTimeout(NetworkConstants.TIME_OUT, TimeUnit.SECONDS);
    httpClient.addInterceptor(aMyInterceptor);
    if (BuildConfig.DEBUG) {
      httpClient.addNetworkInterceptor(new StethoInterceptor());
    }
    return httpClient.build();
  }


  /**
   * Provides a single instance of our SharedPrefs utility class
   *
   * @param aContext The Application context
   * @return SharedPreferencesUtils
   */
  @Singleton
  @Provides
  SharedPreferencesUtils providesSharedPreferencesUtility(@Named(Constants.APP_CONTEXT) Context aContext) {
    return new SharedPreferencesUtils(aContext.getSharedPreferences(Constants.SHARED_PREF_FILE_NAME, MODE_PRIVATE));
  }

  /**
   * Provides the MyTextUtils class used
   *
   * @param aContext - Application Context
   * @return - MyTextUtils Instance
   */
  @Singleton
  @Provides
  MyTextUtils providesMyTextUtils(@Named(Constants.APP_CONTEXT) Context aContext) {
    return new MyTextUtils(aContext);
  }

  /**
   * Provides the ImageUtils class used
   *
   * @return - ImageUtils Instance
   */
  @Singleton
  @Provides
  ImageUtils providesImageUtils() {
    return new ImageUtils();
  }

  /**
   * Provides the Application Context
   *
   * @param aApplication The Main application class
   * @return a Context
   */
  @Provides
  @Named(Constants.APP_CONTEXT)
  Context providesApplicationContext(Application aApplication) {
    return aApplication.getApplicationContext();
  }


  /**
   * Provides our custom MyInterceptor that has network specific data for managing all outgoing network calls
   *
   * @param aNetworkManager The Network Manager
   * @return MyInterceptor
   */
  @Singleton
  @Provides
  MyInterceptor providesInterceptor(NetworkManager aNetworkManager) {
    return new MyInterceptor(aNetworkManager);
  }

  /**
   * Provides a single instance of the Network manager used by MyInterceptor
   *
   * @return a NetworkManager
   */
  @Singleton
  @Provides
  NetworkManager providesNetworkManager() {
    return new NetworkManager();
  }

  /**
   * Provides a single instance of the LocalBroadcastManager
   *
   * @return LocalBroadcastManager
   */
  @Singleton
  @Provides
  LocalBroadcastManager providesLocalBroadcastManager(@Named(Constants.APP_CONTEXT) Context aContext) {
    return LocalBroadcastManager.getInstance(aContext);
  }
}
