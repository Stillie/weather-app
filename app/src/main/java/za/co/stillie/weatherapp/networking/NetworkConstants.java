package za.co.stillie.weatherapp.networking;

public class NetworkConstants {

  //region Base URLS and Auth Keys
  static final String WS_URL = "http://api.openweathermap.org";
  static final String WS_API_KEY = "f6143d05a0261baf2b103e3bbfd9c62b";
  //endregion

  //region Web Services
  static final String WS_LOCATION_WEATHER = "/data/2.5/weather";
  //endregion

  public static final int TIME_OUT = 60;
}


