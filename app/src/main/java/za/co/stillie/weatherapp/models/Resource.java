package za.co.stillie.weatherapp.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import okhttp3.Headers;
import za.co.stillie.weatherapp.enums.ResourceStatus;
import za.co.stillie.weatherapp.networking.ApiResponse;

import static za.co.stillie.weatherapp.enums.ResourceStatus.ERROR;
import static za.co.stillie.weatherapp.enums.ResourceStatus.LOADING;
import static za.co.stillie.weatherapp.enums.ResourceStatus.SUCCESS;


public class Resource<T> {

  /**
   * The status of the network request (Loading, Error, Success)
   */
  @NonNull
  private final ResourceStatus mResourceStatus;

  /**
   * The HTTP error code for the request
   */
  private final int mCode;

  /**
   * A generic data object. This will always be something served back to a ViewModel,
   * and is a locally stored object (I.E from a DB) and never something from the API request
   */
  @Nullable
  private final T mResponse;

  /**
   * HTTP Headers that are part of the network request
   */
  @Nullable
  private Headers mHeaders;

  /**
   * A constructor to setup this class, for an error state
   *
   * @param aCode   The HTTP status code
   */
  private Resource(int aCode) {

    mCode = aCode;
    mResourceStatus = ERROR;
    mResponse = null;
  }

  /**
   * A constructor that sets up this class for a success state
   *
   * @param aData        he locally available data (Either from a DB or some other case)
   * @param aApiResponse The API response from the network request. This object will contain the specific
   *                     data for that request - eg: see LoginResponse - which would be part of this object
   */
  private Resource(@Nullable T aData, @NonNull ApiResponse<T> aApiResponse) {
    mCode = aApiResponse.getCode();
    mHeaders = aApiResponse.getHeaders();
    mResourceStatus = SUCCESS;
    mResponse = aData != null ? aData : aApiResponse.getBody();
  }

  /**
   * A constructor to setup this class for a success state - for when we serve a resource as a response
   * back to a ViewModel, and the source is the DB and not a network request
   *
   * @param aData           The locally available data (Either from a DB or some other case)
   * @param aResourceStatus The status of the request (Most of the time, this will be Success or Loading )
   * @param aCode           The HTTP status code, either a 200 (Success) or a 0 (Loading)
   */
  private Resource(@NonNull ResourceStatus aResourceStatus, @Nullable T aData, int aCode) {

    mCode = aCode;
    mResourceStatus = aResourceStatus;
    mResponse = aData;
  }

  /**
   * A constructor that sets up this class for a success state
   *  @param aData        he locally available data (Either from a DB or some other case)
   * @param aApiResponse The API response from the network request. This object will contain the specific
   */
  public static <T> Resource<T> success(@Nullable T aData, ApiResponse aApiResponse) {
    return new Resource<>(aData, aApiResponse);
  }

  /**
   * A constructor to setup this class for a success state - for when we serve a resource as a response
   * back to a ViewModel, and the source is the DB and not a network request
   *
   * @param aData The locally available data (Either from a DB or some other case)
   * @param aCode The HTTP status code, either a 200 (Success) or a 0 (Loading)
   * @return an instance of this class
   */
  public static <T> Resource<T> success(@Nullable T aData, int aCode) {
    return new Resource<>(SUCCESS, aData, aCode);
  }

  /**
   * A constructor to setup this class, for an error state
   *
   * @param <T>     The Result type object
   * @param aCode   The HTTP status code
   * @return an instance of this class
   */
  public static <T> Resource<T> errors( int aCode) {
    return new Resource<>(aCode);
  }

  /**
   * A constructor to setup this class, for aa loading state
   *
   * @param <T> The Result type object
   * @return an instance of this class
   */
  public static <T> Resource<T> loading() {
    return new Resource<>(LOADING, null, 0);
  }

  /**
   * Gets the HTTP Headers that are part of the network request
   *
   * @return a Headers object
   */
  @Nullable
  public Headers getHeaders() {
    return mHeaders;
  }

  /**
   * Gets the status of the network request (Loading, Error, Success)
   *
   * @return a ResourceStatus object, with the current status
   */
  @NonNull
  public ResourceStatus getResourceStatus() {
    return mResourceStatus;
  }

  /**
   * Gets the HTTP error code for the request
   *
   * @return a int
   */
  public int getCode() {
    return mCode;
  }

  /**
   * A generic data object. This will always be something served back to a ViewModel,
   * and is a locally stored object (I.E from a DB) and never something from the API request
   *
   * @return a generic data object
   */
  @Nullable
  public T getResponse() {
    return mResponse;
  }
}