package za.co.stillie.weatherapp.application;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

import timber.log.Timber;

/**
 * A Listener class, that will be notified, each time the app moves from the background to
 * the foreground and visa-versa
 */
class AppLifeCycleListener implements LifecycleObserver {

  @OnLifecycleEvent(Lifecycle.Event.ON_START)
  public void onMoveToForeground() {
    Timber.d("App in foreground");
  }
  @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
  public void onMoveToBackground() {
    Timber.d("App in background");
  }
}
