package za.co.stillie.weatherapp.utilities;

import android.content.SharedPreferences;

import javax.inject.Inject;

/**
 * A Singleton class (Using Dagger) that provides a convenient wrapper around SharedPreferences
 * for saving, deleting and retrieving data from the SharedPreferences.
 */
public class SharedPreferencesUtils {

  /**
   * The SDK SharedPreferences class, this class wraps around
   */
  private final SharedPreferences mSharedPreferences;

  /**
   * Dagger is used to inject the SharedPreferences dependency into this class
   *
   * @param aSharedPreferences The injected SharedPreferences
   */
  @Inject
  public SharedPreferencesUtils(SharedPreferences aSharedPreferences) {
    mSharedPreferences = aSharedPreferences;
  }

  /**
   * Saves a String to the system SharedPreferences.
   *
   * @param key   a String value to save the value with
   * @param value a String to save to SharedPreferences
   */
  public void putString(String key, String value) {
    mSharedPreferences.edit().putString(key, value).apply();
  }

  /**
   * Saves a Boolean to the system SharedPreferences.
   *
   * @param key   a String value to save the value with
   * @param value a Boolean to save to SharedPreferences
   */
  public void putBoolean(String key, Boolean value) {
    mSharedPreferences.edit().putBoolean(key, value).apply();
  }

  /**
   * Saves a Int to the system SharedPreferences.
   *
   * @param key   a String value to save the value with
   * @param value a Int to save to SharedPreferences
   */
  public void putInt(String key, int value) {
    mSharedPreferences.edit().putInt(key, value).apply();
  }

  /**
   * Retrieves a String value from the SharedPreferences, using a key
   *
   * @param key a String value to get the value with
   * @return a String value
   */
  public String getString(String key) {
    return mSharedPreferences.getString(key, "");
  }

  /**
   * Retrieves a int value from the SharedPreferences, using a key
   *
   * @param key a String value to get the value with
   * @return a Int value
   */
  public int getInt(String key) {
    return mSharedPreferences.getInt(key, 0);
  }

  /**
   * Retrieves a Boolean value from the SharedPreferences, using a key
   *
   * @param key a String value to get the value with
   * @return a Boolean value
   */
  public boolean getBoolean(String key) {
    return mSharedPreferences.getBoolean(key, false);
  }

  /**
   * Checks the SharedPreferences for the provided key
   *
   * @param key a key to check against
   * @return a boolean, indicating if the key exists or not
   */
  public boolean containsKey(String key) {
    return mSharedPreferences.contains(key);
  }

  /**
   * Removes a key from the SharedPreferences
   *
   * @param key The key to remove
   */
  public void remove(String key) {
    if (mSharedPreferences.contains(key)) {
      mSharedPreferences.edit().remove(key).apply();
    }
  }
}
