package za.co.stillie.weatherapp.interfaces;

/**
 * A HTTP Status enum class, that holds all the
 * HTTP Status codes the app deals with.
 */
public interface HttpStatus {

  int INTERNAL_SERVER_ERROR = 500;
  int UNAUTHORIZED = 401;
  int BAD_REQUEST = 400;
  int FORBIDDEN = 403;
  int UNAVAILABLE = 503;
  int MULTI_CHOICE = 300;
  int NOT_FOUND = 404;
  int OK = 200;
}
