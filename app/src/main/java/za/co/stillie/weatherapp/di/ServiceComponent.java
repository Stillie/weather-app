package za.co.stillie.weatherapp.di;

import dagger.Component;


/**
 * A Service Component scope that we use to inject Services using Dagger
 */
@ServiceScope
@Component(dependencies = AppComponent.class, modules = {ServiceModule.class})
interface ServiceComponent {
}
