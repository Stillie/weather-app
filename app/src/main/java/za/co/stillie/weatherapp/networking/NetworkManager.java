package za.co.stillie.weatherapp.networking;

import javax.inject.Singleton;

@Singleton
public class NetworkManager {

  public NetworkManager() {
  }

  /**
   * Get the URL for the application
   *
   * @return - String of the URL
   */
  String getBaseUrl() {
    return NetworkConstants.WS_URL;
  }
}
