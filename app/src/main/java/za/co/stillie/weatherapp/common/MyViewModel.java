package za.co.stillie.weatherapp.common;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import za.co.stillie.weatherapp.models.Resource;


public class MyViewModel<T> extends ViewModel implements Observer<Resource> {
  private LiveData<Resource> mLiveData;

  protected void observeLiveData(LiveData<Resource> aResourceLiveData){
    mLiveData = aResourceLiveData;
    mLiveData.observeForever(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    if (mLiveData != null){
      mLiveData.removeObserver(this);
    }
  }

  @Override
  public void onChanged(@Nullable Resource aResource) {

  }
}
