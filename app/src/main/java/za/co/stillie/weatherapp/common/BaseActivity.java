package za.co.stillie.weatherapp.common;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.MutableLiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;
import za.co.stillie.weatherapp.BuildConfig;
import za.co.stillie.weatherapp.Constants;
import za.co.stillie.weatherapp.R;
import za.co.stillie.weatherapp.utilities.ImageUtils;
import za.co.stillie.weatherapp.utilities.MyTextUtils;

public abstract class BaseActivity extends AppCompatActivity implements LifecycleOwner, HasActivityInjector, HasSupportFragmentInjector {

  @Inject
  DispatchingAndroidInjector<Activity> mDispatchingAndroidInjectorActivity;

  @Inject
  DispatchingAndroidInjector<Fragment> mDispatchingAndroidInjectorFragment;

  // Text Util Class
  @Inject
  MyTextUtils mMyTextUtils;

  // Image Util Class
  @Inject
  ImageUtils mImageUtils;

  // MutableLiveData Object to monitor when the devices location has been retrieved
  private MutableLiveData<Location> mLocationLiveData = new MutableLiveData<>();

  // LifeCycle to know when the app is in the background or not
  private LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);

  // Request code used when the user is requesting location permissions
  public final int PERMISSIONS_REQUEST_CODE = 1099;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    registerBroadcastReceivers();
    if (savedInstanceState == null) {
      startLocationPermissionRequest();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterBroadcastReceivers();
  }

  /**
   * Gets the ImageUtils Class
   *
   * @return - ImageUtils
   */
  public ImageUtils getImageUtils() {
    return mImageUtils;
  }


  /**
   * Gets the MyTextUtils Class
   *
   * @return - MyTextUtils
   */
  public MyTextUtils getMyTextUtils() {
    return mMyTextUtils;
  }

  private void unregisterBroadcastReceivers() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mShowDialogReceiver);
  }

  @Override
  public DispatchingAndroidInjector<Activity> activityInjector() {
    return mDispatchingAndroidInjectorActivity;
  }

  @Override
  public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
    return mDispatchingAndroidInjectorFragment;
  }

  @Override
  @Nonnull
  public LifecycleRegistry getLifecycle() {
    return mLifecycleRegistry;
  }

  /**
   * Register the broadcast Receivers
   */
  private void registerBroadcastReceivers() {
    LocalBroadcastManager.getInstance(this).registerReceiver(mShowDialogReceiver, new IntentFilter(Constants.Local_Broadcast_Show_Dialog));
  }

  /**
   * Show a dialog
   *
   * @param aMessage         - Message
   * @param aButtonText      - Button Text
   * @param aDialogInterface - OnClickListener
   */
  public void showDialog(String aMessage, String aButtonText, DialogInterface.OnClickListener aDialogInterface) {
    if (aDialogInterface == null) {
      aDialogInterface = (aDialogInterface1, aI) -> {
        // do nothing
      };
    }
    if (TextUtils.isEmpty(aButtonText)) {
      aButtonText = getString(R.string.okay_text);
    }
    new AlertDialog.Builder(this).setMessage(aMessage).setPositiveButton(aButtonText, aDialogInterface).show();
  }

  /**
   * Request Location Permissions
   */
  protected void startLocationPermissionRequest() {
    ActivityCompat.requestPermissions(this,
            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
            PERMISSIONS_REQUEST_CODE);
  }

  /**
   * Provides a simple way of getting a device's location and is well suited for
   * applications that do not require a fine-grained location and that do not need location
   * updates. Gets the best and most recent location currently available, which may be null
   * in rare cases when a location is not available.
   * <p>
   * Note: this method should be called after location permission has been granted.
   */
  @SuppressWarnings("MissingPermission")
  private void getLastLocation() {
    FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    fusedLocationClient.getLastLocation().addOnSuccessListener(this,
            aLocation -> {
              if (aLocation != null) {
                mLocationLiveData.setValue(aLocation);
              } else {
                showDialog(getString(R.string.generic_error_message), null, null);
              }
            }

    );
  }

  /**
   * When the Permissions Request has been completed this is called
   *
   * @param requestCode  - Request code
   * @param permissions  - Perms
   * @param grantResults - Grant Results
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if (requestCode == PERMISSIONS_REQUEST_CODE) {
      if (grantResults.length <= 0) {
        // If user interaction was interrupted, the permission request is cancelled and you
        // receive empty arrays.
      } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permission granted.
        getLastLocation();
      } else {
        // Permission denied.

        // Request permission. It's possible this can be auto answered if device policy
        // sets the permission in a given state or the user denied the permission
        // previously and checked "Never ask again".
        showLocationErrorDialog();
      }
    }
  }

  /**
   * Show the Location Perms error dialog
   */
  private void showLocationErrorDialog() {
    showDialog(getString(R.string.app_name) + getString(R.string.location_denied_message), getString(R.string.go_to_settings_text), (aDialogInterface, aI) -> {
      // Build intent that displays the App settings screen.
      Intent intent = new Intent();
      intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
      Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
      intent.setData(uri);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
    });
  }

  /**
   * Get the Mutable Live Data object to monitor the perms results
   *
   * @return - Location Live Data
   */
  public MutableLiveData<Location> getLocationLiveData() {
    return mLocationLiveData;
  }

  /**
   * Show a dialog with the receiver
   */
  private final BroadcastReceiver mShowDialogReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context aContext, Intent aIntent) {
      showDialog(aIntent.getStringExtra("message"), getString(R.string.okay_text), null);
    }
  };
}
